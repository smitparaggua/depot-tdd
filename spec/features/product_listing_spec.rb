require "rails_helper"
require 'features/pages/product_index_page'

feature 'Product listing', type: :feature do
  scenario 'User sees the products' do
    product = create :product
    page = ProductIndexPage.new
    page.visit_page
    expect(page).to have_product(product)
    expect(page.status_code).to eq(200)
  end
end
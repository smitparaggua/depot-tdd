class ProductIndexPage
  include Capybara::DSL
  include Rails.application.routes.url_helpers

  def visit_page
    visit products_path
    self
  end

  def has_product?(product)
    element = find("##{product.id}")
    element.has_content?(product.title) &&
      element.has_content?(product.description.truncate 80) &&
      element.has_link?('Show', product_path(product))
  end
end
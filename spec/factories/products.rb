FactoryGirl.define do
  factory :product do
    title Faker::Name.title
    description Faker::Lorem.paragraph
    image_url Faker::Company.logo
    price Faker::Commerce.price
  end
end

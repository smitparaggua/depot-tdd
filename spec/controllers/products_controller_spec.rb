require 'rails_helper'

RSpec.describe ProductsController, type: :controller do
  describe 'GET #index' do
    before :example do |example|
      product = create :product
      get :index unless example.metadata[:skip_send_request]
    end

    it 'assigns products to @product', skip_send_request: true do
      product = create :product
      get :index
      expect(assigns :products).to include(product)
    end

    it { is_expected.to respond_with :success }
    it { is_expected.to render_template 'index' }
  end
end
